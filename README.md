About this repo
-------------------------

This site (this repo) is now served on casper.mupdf.com, the **new mupdf.com site** is served via Vercel by the Artifex Cloud Platform team. Source code is: https://github.com/ArtifexSoftware/mupdf.com


We still need casper.mupdf.com, because we want to easily update release details.
Therefore we still alias to the releases files and a few other things as follows:


The **new mupdf.com site**  (the one hosted on Vercel) consumes these files:

- `releases/releases.json`
- `releases/history-list.html`
- `releases/cve-list.json`


The **new mupdf.com site** (the one hosted on Vercel) also points to:

- all the content in `wasm/demo`
- `docs/mupdf_explored.pdf`
- `downloads/archive`


In summary, we need the stuff in :

- `releases`
- `wasm`
- `docs`
- `downloads`


To update with a new release
------------------------------

As usual put the binaries in `/downloads/archive`, then update:

- `releases/releases.json`
- `releases/history-list.html`
- `releases/cve-list.json` (if required)




